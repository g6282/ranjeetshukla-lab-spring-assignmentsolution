## My SQL QUERIES
```sh
CREATE DATABASE studentmanagement;
use studentmanagement;
INSERT INTO roles VALUES(1, 'ADMIN'); 
INSERT INTO roles VALUES(2, 'USER');
INSERT INTO users VALUES(1, '$2a$12$XPHcez3Jt8JND.l/gZgoFuabP9aVoiLVz5UrdmumHLnnuIElW89JW', 'ranjeet'); #password - 12345
INSERT INTO users VALUES(2, '$2a$12$XPHcez3Jt8JND.l/gZgoFuabP9aVoiLVz5UrdmumHLnnuIElW89JW', 'rahul'); #password - 12345
INSERT INTO users_roles VALUES(1, 1); 
INSERT INTO users_roles VALUES(2, 2);

SELECT * FROM users_roles;
SELECT * FROM users;
SELECT * FROM student;
SELECT * FROM roles;
```
